package io.gitlab.teammycelium.myntal.core.common

import com.eclipsesource.json.JsonObject
import io.gitlab.teammycelium.lib.core.api.IConfig
import io.gitlab.teammycelium.lib.core.api.IModule
import io.gitlab.teammycelium.lib.core.common.Config
import io.gitlab.teammycelium.lib.core.common.monads.Option
import io.gitlab.teammycelium.lib.core.common.monads.mapSome
import io.gitlab.teammycelium.myntal.material.common.MaterialModule
import net.minecraft.block.Block
import net.minecraft.creativetab.CreativeTabs
import net.minecraft.init.Blocks
import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import net.minecraftforge.client.event.ColorHandlerEvent
import net.minecraftforge.client.event.ModelRegistryEvent
import net.minecraftforge.common.MinecraftForge
import net.minecraftforge.event.RegistryEvent
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.common.SidedProxy
import net.minecraftforge.fml.common.event.FMLInitializationEvent
import net.minecraftforge.fml.common.event.FMLInterModComms
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent
import net.minecraftforge.fml.relauncher.Side
import net.minecraftforge.fml.relauncher.SideOnly
import org.apache.logging.log4j.LogManager

@Mod(
    modid = Myntal.MOD_ID,
    name = "Myntal",
    version = "1.0.0.0",
    dependencies = "required-after:forge@[14.21.1.2395,);required-after:mycelium_lib@[1.0.0.0,)"
)
class Myntal : IConfig {
    companion object {
        @JvmStatic
        val TAB = MyntalTab()

        const val MOD_ID = "myntal"

        val modules: List<IModule> = listOf(
            MaterialModule()
        )

        var config: Option<JsonObject> = Option.None()

        @JvmStatic
        @SidedProxy(
            modId = MOD_ID,
            clientSide = "io.gitlab.teammycelium.myntal.core.client.ClientProxy",
            serverSide = "io.gitlab.teammycelium.myntal.core.server.ServerProxy"
        )
        private lateinit var proxy: IProxy
    }

    private val logger = LogManager.getLogger("Myntal")

    @Mod.EventHandler
    fun preInit(event: FMLPreInitializationEvent) {
        logger.info("preInit")

        Config(name()).load(listOf(this).plus(modules)).mapSome {
            logger.error("Issue loading `${name()}.json`", it)
        }

        modules.forEach { it.preInit(event) }

        proxy.preInit(event)

        MinecraftForge.EVENT_BUS.register(this)
    }

    @Mod.EventHandler
    fun init(event: FMLInitializationEvent) {
        logger.info("init")

        modules.forEach { it.init(event) }

        proxy.init(event)
    }

    @Mod.EventHandler
    fun postInit(event: FMLPostInitializationEvent) {
        logger.info("postInit")

        modules.forEach { it.postInit(event) }

        proxy.postInit(event)
    }

    @Mod.EventHandler
    fun messageReceived(event: FMLInterModComms.IMCEvent) {
        logger.info("messageReceived")

        event.messages.forEach {
            logger.info("imc: ${it.key}")
        }
    }

    @SubscribeEvent
    fun registerBlocks(event: RegistryEvent.Register<Block>) {
        logger.info("registerBlocks: start")

        val registry = event.registry

        modules.forEach { it.registerBlocks(registry) }

        logger.info("registerBlocks: stop")
    }

    @SubscribeEvent
    fun registerItems(event: RegistryEvent.Register<Item>) {
        logger.info("registerItems: start")

        val registry = event.registry

        modules.forEach { it.registerItems(registry) }

        logger.info("registerItems: stop")
    }

    @SubscribeEvent
    @SideOnly(Side.CLIENT)
    fun registerRenders(@Suppress("UNUSED_PARAMETER") registry: ModelRegistryEvent) {
        logger.info("registerRenders: start")

        modules.forEach { it.registerRenders() }

        logger.info("registerRenders: stop")
    }

    @SubscribeEvent
    @SideOnly(Side.CLIENT)
    fun registerBlockColors(event: ColorHandlerEvent.Block) {
        logger.info("registerBlockColors: start")

        modules.forEach { it.registerBlockColors(event) }

        logger.info("registerBlockColors: stop")
    }

    @SubscribeEvent
    @SideOnly(Side.CLIENT)
    fun registerItemColors(event: ColorHandlerEvent.Item) {
        logger.info("registerItemColors: start")

        modules.forEach { it.registerItemColors(event) }

        logger.info("registerItemColors: stop")
    }

    override fun name(): String {
        return "myntal"
    }

    override fun createConfig(): JsonObject {
        logger.info("createConfig")

        return JsonObject()
    }

    override fun setConfig(obj: JsonObject) {
        logger.info("setConfig")

        config = Option.Some(obj)
    }

    class MyntalTab : CreativeTabs("Myntal") {
        @SideOnly(Side.CLIENT)
        override fun getTabIconItem(): ItemStack {
            return ItemStack(Item.getItemFromBlock(Blocks.COAL_BLOCK))
        }
    }
}
