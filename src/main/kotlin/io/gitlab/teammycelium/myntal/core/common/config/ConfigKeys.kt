package io.gitlab.teammycelium.myntal.core.common.config

object ConfigKeys {
    const val COLOR = "color"

    const val BLOCKS = "blocks"
    const val ITEMS = "items"
    const val GENERATION = "generation"
    const val TOOLS = "tools"

    const val GENERATION_DIMENSION_WHITELIST = "dimensionWhitelist"
    const val GENERATION_MIN_HEIGHT = "minHeight"
    const val GENERATION_MAX_HEIGHT = "maxHeight"
    const val GENERATION_SPAWN_TRIES = "spawnTries"
    const val GENERATION_VEIN_SIZE = "veinSize"

    const val ENABLE_GENERATION = "enableGeneration"
}