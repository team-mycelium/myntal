package io.gitlab.teammycelium.myntal.core.common.config

import com.eclipsesource.json.JsonObject
import io.gitlab.teammycelium.lib.core.common.Config
import io.gitlab.teammycelium.lib.core.common.monads.Option
import io.gitlab.teammycelium.lib.core.common.monads.andThen
import io.gitlab.teammycelium.lib.core.common.monads.map
import io.gitlab.teammycelium.lib.core.common.serde.obj
import io.gitlab.teammycelium.lib.core.common.serde.tryBoolean
import io.gitlab.teammycelium.lib.core.common.serde.tryObject

class Items(
    var enabled: Boolean = true,
    var enableChunk: Boolean = true,
    var enableDust: Boolean = true,
    var enableIngot: Boolean = true,
    var enableNugget: Boolean = true
) {
    companion object {
        const val ENABLE_CHUNK = "enableChunk"
        const val ENABLE_DUST = "enableDust"
        const val ENABLE_INGOT = "enableIngot"
        const val ENABLE_NUGGET = "enableNugget"

        fun read(obj: JsonObject): Option<Items> {
            return obj.get(Config.Keys.ENABLED)
                .tryBoolean()
                .andThen { enabled ->
                    obj.get(Config.Keys.PARAMETERS)
                        .tryObject()
                        .andThen { parameters ->
                            parameters.get(ENABLE_CHUNK)
                                .tryBoolean()
                                .andThen { enableChunk ->
                                    parameters.get(ENABLE_DUST)
                                        .tryBoolean()
                                        .andThen { enableDust ->
                                            parameters.get(ENABLE_INGOT)
                                                .tryBoolean()
                                                .andThen { enableIngot ->
                                                    parameters.get(ENABLE_NUGGET)
                                                        .tryBoolean()
                                                        .map { enableNugget ->
                                                            Items(
                                                                enabled,
                                                                enableChunk,
                                                                enableDust,
                                                                enableIngot,
                                                                enableNugget
                                                            )
                                                        }
                                                }
                                        }
                                }
                        }
                }
        }
    }

    fun write(): JsonObject {
        return obj {
            Config.Keys.ENABLED to enabled
            Config.Keys.PARAMETERS to obj {
                ENABLE_CHUNK to enableChunk
                ENABLE_DUST to enableDust
                ENABLE_INGOT to enableIngot
                ENABLE_NUGGET to enableNugget
            }
        }
    }
}