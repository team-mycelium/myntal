package io.gitlab.teammycelium.myntal.core.common.config

import com.eclipsesource.json.JsonObject
import io.gitlab.teammycelium.lib.core.common.Config
import io.gitlab.teammycelium.lib.core.common.monads.Option
import io.gitlab.teammycelium.lib.core.common.monads.andThen
import io.gitlab.teammycelium.lib.core.common.monads.map
import io.gitlab.teammycelium.lib.core.common.serde.obj
import io.gitlab.teammycelium.lib.core.common.serde.tryBoolean
import io.gitlab.teammycelium.lib.core.common.serde.tryFloat
import io.gitlab.teammycelium.lib.core.common.serde.tryObject

class Blocks {
    var enabled: Boolean = true

    fun enabled(value: Boolean): Blocks {
        enabled = value

        return this
    }

    var enableBlock: Boolean = true

    fun enableBlock(value: Boolean): Blocks {
        enableBlock = value

        return this
    }

    var enableOre: Boolean = true

    fun enableOre(value: Boolean): Blocks {
        enableOre = value

        return this
    }

    var hardness: Float = 1.0F

    fun hardness(value: Float): Blocks {
        hardness = value

        return this
    }

    var resistance: Float = 1.0F

    fun resistance(value: Float): Blocks {
        resistance = value

        return this
    }

    companion object {
        const val ENABLE_BLOCK = "enableBlock"
        const val ENABLE_ORE = "enableOre"

        const val HARDNESS = "hardness"
        const val RESISTANCE = "resistance"

        fun read(obj: JsonObject): Option<Blocks> {
            return obj.get(Config.Keys.ENABLED)
                .tryBoolean()
                .andThen { enabled ->
                    obj.get(Config.Keys.PARAMETERS)
                        .tryObject()
                        .andThen { parameters ->
                            parameters.get(ENABLE_BLOCK)
                                .tryBoolean()
                                .andThen { enableBlock ->
                                    parameters.get(ENABLE_ORE)
                                        .tryBoolean()
                                        .andThen { enableOre ->
                                            parameters.get(HARDNESS)
                                                .tryFloat()
                                                .andThen { hardness ->
                                                    parameters.get(RESISTANCE)
                                                        .tryFloat()
                                                        .map { resistance ->
                                                            Blocks()
                                                                .enabled(enabled)
                                                                .enableBlock(enableBlock)
                                                                .enableOre(enableOre)
                                                                .hardness(hardness)
                                                                .resistance(resistance)
                                                        }
                                                }

                                        }
                                }
                        }
                }
        }
    }

    fun write(): JsonObject {
        return obj {
            Config.Keys.ENABLED to enabled
            Config.Keys.PARAMETERS to obj {
                ENABLE_BLOCK to enableBlock
                ENABLE_ORE to enableOre
                HARDNESS to hardness
                RESISTANCE to resistance
            }
        }
    }
}