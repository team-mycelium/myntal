package io.gitlab.teammycelium.myntal.core.common.config

import com.eclipsesource.json.JsonObject
import io.gitlab.teammycelium.lib.core.common.Config
import io.gitlab.teammycelium.lib.core.common.monads.Option
import io.gitlab.teammycelium.lib.core.common.monads.andThen
import io.gitlab.teammycelium.lib.core.common.monads.map
import io.gitlab.teammycelium.lib.core.common.serde.obj
import io.gitlab.teammycelium.lib.core.common.serde.tryBoolean
import io.gitlab.teammycelium.lib.core.common.serde.tryFloat
import io.gitlab.teammycelium.lib.core.common.serde.tryInt
import io.gitlab.teammycelium.lib.core.common.serde.tryObject

class Tools(
    var enabled: Boolean = true,
    var enableAxe: Boolean = true,
    var enableHoe: Boolean = true,
    var enablePickaxe: Boolean = true,
    var enableShovel: Boolean = true,
    var enableSword: Boolean = true
) {
    
    lateinit var material: Material

    fun material(
        harvest: Int,
        uses: Int,
        efficiency: Float,
        damage: Float,
        enchantability: Int,
        speed: Float
    ): Tools {
        this.material = Material(harvest, uses, efficiency, damage, enchantability, speed)

        return this
    }

    fun material(material: Material): Tools {
        this.material = material

        return this
    }

    companion object {
        const val ENABLE_AXE = "enableAxe"
        const val ENABLE_HOE = "enableHoe"
        const val ENABLE_PICKAXE = "enablePickaxe"
        const val ENABLE_SHOVEL = "enableShovel"
        const val ENABLE_SWORD = "enableSword"

        const val MATERIAL = "material"
        const val DAMAGE = "damage"
        const val EFFICIENCY = "efficiency"
        const val ENCHANTABILITY = "enchantability"
        const val HARVEST = "harvest"
        const val SPEED = "speed"
        const val USES = "uses"

        fun read(obj: JsonObject): Option<Tools> {
            return obj.get(Config.Keys.ENABLED)
                .tryBoolean()
                .andThen { enabled ->
                    obj.get(Config.Keys.PARAMETERS)
                        .tryObject()
                        .andThen { parameters ->
                            parameters.get(ENABLE_AXE)
                                .tryBoolean()
                                .andThen { enableAxe ->
                                    parameters.get(ENABLE_HOE)
                                        .tryBoolean()
                                        .andThen { enableHoe ->
                                            parameters.get(ENABLE_PICKAXE)
                                                .tryBoolean()
                                                .andThen { enablePickaxe ->
                                                    parameters.get(ENABLE_SHOVEL)
                                                        .tryBoolean()
                                                        .andThen { enableShovel ->
                                                            parameters.get(ENABLE_SWORD)
                                                                .tryBoolean()
                                                                .andThen { enableSword ->
                                                                    parameters.get(MATERIAL)
                                                                        .tryObject()
                                                                        .andThen(Material.Companion::read)
                                                                        .map { material ->
                                                                            Tools(
                                                                                enabled,
                                                                                enableAxe,
                                                                                enableHoe,
                                                                                enablePickaxe,
                                                                                enableShovel,
                                                                                enableSword
                                                                            ).material(material)
                                                                        }
                                                                }
                                                        }
                                                }
                                        }
                                }
                        }
                }
        }
    }

    fun write(): JsonObject {
        return obj {
            Config.Keys.ENABLED to enabled
            Config.Keys.PARAMETERS to obj {
                ENABLE_AXE to enableAxe
                ENABLE_HOE to enableHoe
                ENABLE_PICKAXE to enablePickaxe
                ENABLE_SHOVEL to enableShovel
                ENABLE_SWORD to enableSword
                MATERIAL to material.write()
            }
        }
    }

    class Material(
        var harvest: Int,
        var uses: Int,
        var efficiency: Float,
        var damage: Float,
        var enchantability: Int,
        var speed: Float
    ) {
        companion object {
            fun read(obj: JsonObject): Option<Material> {
                return obj.get(HARVEST)
                    .tryInt()
                    .andThen { harvest ->
                        obj.get(USES)
                            .tryInt()
                            .andThen { uses ->
                                obj.get(EFFICIENCY)
                                    .tryFloat()
                                    .andThen { efficiency ->
                                        obj.get(DAMAGE)
                                            .tryFloat()
                                            .andThen { damage ->
                                                obj.get(SPEED)
                                                    .tryFloat()
                                                    .andThen { speed ->
                                                        obj.get(ENCHANTABILITY)
                                                            .tryInt()
                                                            .map { enchantability ->
                                                                Material(
                                                                    harvest,
                                                                    uses,
                                                                    efficiency,
                                                                    damage,
                                                                    enchantability,
                                                                    speed
                                                                )
                                                            }
                                                    }
                                            }
                                    }
                            }
                    }
            }
        }

        fun write(): JsonObject {
            return obj {
                HARVEST to harvest
                USES to uses
                EFFICIENCY to efficiency
                DAMAGE to damage
                ENCHANTABILITY to enchantability
                SPEED to speed
            }
        }
    }
}