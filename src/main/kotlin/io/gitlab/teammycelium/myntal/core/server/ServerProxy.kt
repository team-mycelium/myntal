package io.gitlab.teammycelium.myntal.core.server

import io.gitlab.teammycelium.lib.core.common.logger
import io.gitlab.teammycelium.myntal.core.common.IProxy
import io.gitlab.teammycelium.myntal.core.common.Myntal.Companion.modules
import net.minecraftforge.fml.common.event.FMLInitializationEvent
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent

class ServerProxy: IProxy {
    override fun preInit(event: FMLPreInitializationEvent) {
        logger.info("serverPreInit: start")

        modules.forEach { it.serverPreInit(event) }

        logger.info("serverPreInit: stop")
    }

    override fun init(event: FMLInitializationEvent) {
        logger.info("serverInit: start")

        modules.forEach { it.serverInit(event) }

        logger.info("serverInit: stop")
    }

    override fun postInit(event: FMLPostInitializationEvent) {
        logger.info("serverPostInit: start")

        modules.forEach { it.serverPostInit(event) }

        logger.info("serverPostInit: stop")
    }
}