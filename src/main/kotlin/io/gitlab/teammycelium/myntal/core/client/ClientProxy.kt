package io.gitlab.teammycelium.myntal.core.client

import io.gitlab.teammycelium.lib.core.common.logger
import io.gitlab.teammycelium.myntal.core.common.IProxy
import io.gitlab.teammycelium.myntal.core.common.Myntal.Companion.modules
import net.minecraftforge.fml.common.event.FMLInitializationEvent
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent

class ClientProxy: IProxy {
    override fun preInit(event: FMLPreInitializationEvent) {
        logger.info("clientPreInit: start")

        modules.forEach { it.clientPreInit(event) }

        logger.info("clientPreInit: stop")
    }

    override fun init(event: FMLInitializationEvent) {
        logger.info("clientInit: start")

        modules.forEach { it.clientInit(event) }

        logger.info("clientInit: stop")
    }

    override fun postInit(event: FMLPostInitializationEvent) {
        logger.info("clientPostInit: start")

        modules.forEach { it.clientPostInit(event) }

        logger.info("clientPostInit: stop")
    }
}