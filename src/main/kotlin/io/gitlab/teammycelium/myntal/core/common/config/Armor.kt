package io.gitlab.teammycelium.myntal.core.common.config

import com.eclipsesource.json.JsonObject
import io.gitlab.teammycelium.lib.core.common.Config
import io.gitlab.teammycelium.lib.core.common.monads.Option
import io.gitlab.teammycelium.lib.core.common.serde.obj

class Armor(
    var enabled: Boolean = true
) {
    companion object {
        const val ENABLE_HELMET = "enableHelmet"
        const val ENABLE_CHESTPLATE = "enableChestplate"
        const val ENABLE_LEGGINGS = "enableLeggings"
        const val ENABLE_BOOTS = "enableBoots"

        fun read(obj: JsonObject): Option<Armor> {
            return Option.None()
        }
    }

    fun write(): JsonObject {
        return obj {
            Config.Keys.ENABLED to enabled
            Config.Keys.PARAMETERS to obj {

            }
        }
    }
}