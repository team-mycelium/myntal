package io.gitlab.teammycelium.myntal.material.common

import com.eclipsesource.json.JsonObject
import io.gitlab.teammycelium.lib.core.client.registerItemRender
import io.gitlab.teammycelium.lib.core.common.Config
import io.gitlab.teammycelium.lib.core.common.blocks.KBlock
import io.gitlab.teammycelium.lib.core.common.generation.Generation
import io.gitlab.teammycelium.lib.core.common.items.KItem
import io.gitlab.teammycelium.lib.core.common.items.tools.KAxe
import io.gitlab.teammycelium.lib.core.common.items.tools.KHoe
import io.gitlab.teammycelium.lib.core.common.items.tools.KPickaxe
import io.gitlab.teammycelium.lib.core.common.items.tools.KShovel
import io.gitlab.teammycelium.lib.core.common.items.tools.KSword
import io.gitlab.teammycelium.lib.core.common.monads.Option
import io.gitlab.teammycelium.lib.core.common.monads.andThen
import io.gitlab.teammycelium.lib.core.common.monads.map
import io.gitlab.teammycelium.lib.core.common.monads.mapOr
import io.gitlab.teammycelium.lib.core.common.monads.mapSome
import io.gitlab.teammycelium.lib.core.common.serde.obj
import io.gitlab.teammycelium.lib.core.common.serde.tryObject
import io.gitlab.teammycelium.myntal.core.common.config.ConfigKeys
import io.gitlab.teammycelium.myntal.core.common.Myntal
import io.gitlab.teammycelium.myntal.core.common.config.Blocks
import io.gitlab.teammycelium.myntal.core.common.config.Items
import io.gitlab.teammycelium.myntal.core.common.config.Tools
import io.gitlab.teammycelium.myntal.material.api.IMaterial
import net.minecraft.block.Block
import net.minecraft.block.material.Material
import net.minecraft.block.state.IBlockState
import net.minecraft.client.renderer.color.IBlockColor
import net.minecraft.client.renderer.color.IItemColor
import net.minecraft.item.Item
import net.minecraft.item.ItemBlock
import net.minecraft.item.ItemHoe
import net.minecraft.item.ItemStack
import net.minecraft.item.ItemSword
import net.minecraft.item.ItemTool
import net.minecraft.util.BlockRenderLayer
import net.minecraft.util.math.BlockPos
import net.minecraft.world.IBlockAccess
import net.minecraftforge.client.event.ColorHandlerEvent
import net.minecraftforge.common.util.EnumHelper
import net.minecraftforge.fml.common.event.FMLInitializationEvent
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent
import net.minecraftforge.registries.IForgeRegistry
import org.apache.logging.log4j.LogManager

class BaseMaterial(private val cfg: Builder) : IMaterial {
    private val logger = LogManager.getLogger("Myntal/Material/Common/${cfg.name.capitalize()}")!!

    private var colorHandler: Option<ColorHandler> = Option.None()

    private var config: Option<JsonObject> = Option.None()

    private var blocks: Option<Blocks> = Option.None()
    private var items: Option<Items> = Option.None()
    private var generation: Option<Generation> = Option.None()
    private var tools: Option<Tools> = Option.None()

    private var toolMaterial: Option<Item.ToolMaterial> = Option.None()

    private var block: Option<KBlock> = Option.None()
    private var ore: Option<KBlock> = Option.None()

    private var chunk: Option<KItem> = Option.None()
    private var dust: Option<KItem> = Option.None()
    private var ingot: Option<KItem> = Option.None()
    private var nugget: Option<KItem> = Option.None()

    private var axe: Option<KAxe> = Option.None()
    private var hoe: Option<KHoe> = Option.None()
    private var pickaxe: Option<KPickaxe> = Option.None()
    private var shovel: Option<KShovel> = Option.None()
    private var sword: Option<KSword> = Option.None()

    override fun name(): String {
        return cfg.name
    }

    override fun createConfig(): JsonObject {
        logger.info("createConfig")

        return obj {
            ConfigKeys.ENABLE_CHUNK to cfg.enableChunk
            ConfigKeys.ENABLE_DUST to cfg.enableDust
            ConfigKeys.ENABLE_INGOT to cfg.enableIngot
            ConfigKeys.ENABLE_NUGGET to cfg.enableNugget
            ConfigKeys.ENABLE_GENERATION to cfg.enableGeneration
            ConfigKeys.COLOR to cfg.color
            ConfigKeys.BLOCKS to cfg.blocks.write()
            ConfigKeys.ITEMS to cfg.items.write()
            ConfigKeys.GENERATION to cfg.generation.write()
            ConfigKeys.TOOLS to cfg.tools.write()
        }
    }

    override fun setConfig(obj: JsonObject) {
        logger.info("setConfig")

        blocks = obj.get(ConfigKeys.BLOCKS)
            .tryObject()
            .andThen(Blocks.Companion::read)

        items = obj.get(ConfigKeys.ITEMS)
            .tryObject()
            .andThen(Items.Companion::read)

        generation = obj.get(Config.Keys.GENERATION)
            .tryObject()
            .andThen(Generation.Companion::read)

        tools = obj.get(ConfigKeys.TOOLS)
            .tryObject()
            .andThen(Tools.Companion::read)

        toolMaterial = tools.map { tools ->
            EnumHelper.addToolMaterial(
                "${Myntal.MOD_ID}_${name()}_material",
                tools.material.harvest,
                tools.material.uses,
                tools.material.efficiency,
                tools.material.damage,
                tools.material.enchantability
            )!!
        }

        config = Option.Some(obj)
    }

    override fun preInit(event: FMLPreInitializationEvent) {
        logger.info("preInit")

        block = blocks.andThen {
            (it.enabled && it.enableBlock).map {
                KBlock(Material.IRON)
                    .name("${name()}_block", Myntal.MOD_ID)
                    .hardness(it.hardness)
                    .resistance(it.resistance)
                    .creativeTab(Myntal.TAB)
            }
        }

        ore = blocks.andThen {
            (it.enabled && it.enableBlock).map {
                KBlock(Material.ROCK)
                    .name("${name()}_ore", Myntal.MOD_ID)
                    .blockLayer(BlockRenderLayer.CUTOUT)
                    .hardness(it.hardness)
                    .resistance(it.resistance)
                    .creativeTab(Myntal.TAB)
            }
        }

        chunk = items.andThen {
            (it.enabled && it.enableChunk).map {
                KItem()
                    .name("${name()}_chunk", Myntal.MOD_ID)
                    .creativeTab(Myntal.TAB)
            }
        }

        dust = items.andThen {
            (it.enabled && it.enableDust).map {
                KItem()
                    .name("${name()}_dust", Myntal.MOD_ID)
                    .creativeTab(Myntal.TAB)
            }
        }

        ingot = items.andThen {
            (it.enabled && it.enableIngot).map {
                KItem()
                    .name("${name()}_ingot", Myntal.MOD_ID)
                    .creativeTab(Myntal.TAB)
            }
        }

        nugget = items.andThen {
            (it.enabled && it.enableNugget).map {
                KItem()
                    .name("${name()}_nugget", Myntal.MOD_ID)
                    .creativeTab(Myntal.TAB)
            }
        }

        axe = tools.andThen { tools ->
            toolMaterial.andThen { material ->
                (tools.enabled && tools.enableAxe).map {
                    KAxe(material, 8.0F, tools.material.speed)
                        .name("${name()}_axe", Myntal.MOD_ID)
                        .creativeTab(Myntal.TAB)
                }
            }
        }

        hoe = tools.andThen { tools ->
            toolMaterial.andThen { material ->
                (tools.enabled && tools.enableHoe).map {
                    KHoe(material)
                        .name("${name()}_hoe", Myntal.MOD_ID)
                        .creativeTab(Myntal.TAB)
                }
            }
        }

        pickaxe = tools.andThen { tools ->
            toolMaterial.andThen { material ->
                (tools.enabled && tools.enablePickaxe).map {
                    KPickaxe(material)
                        .name("${name()}_pickaxe", Myntal.MOD_ID)
                        .creativeTab(Myntal.TAB)
                }
            }
        }

        shovel = tools.andThen { tools ->
            toolMaterial.andThen { material ->
                (tools.enabled && tools.enableShovel).map {
                    KShovel(material)
                        .name("${name()}_shovel", Myntal.MOD_ID)
                        .creativeTab(Myntal.TAB)
                }
            }
        }

        sword = tools.andThen { tools ->
            toolMaterial.andThen { material ->
                (tools.enabled && tools.enableSword).map {
                    KSword(material)
                        .name("${name()}_sword", Myntal.MOD_ID)
                        .creativeTab(Myntal.TAB)
                }
            }
        }
    }

    override fun init(event: FMLInitializationEvent) {
        logger.info("init")

//        config.getObj(ConfigKeys.GENERATION).mapSome {
//            if (config.generation() && config.ore()) {
//                ore.mapSome { ore ->
//                    GameRegistry.registerWorldGenerator(OldGeneration(logger, it, ore), 0)
//                }
//            }
//        }
    }

    override fun postInit(event: FMLPostInitializationEvent) {
        logger.info("postInit")
    }

    override fun clientPreInit(event: FMLPreInitializationEvent) {
        logger.info("clientPreInit")

        colorHandler = Option.Some(ColorHandler(config.mapOr(cfg.color) { it.getInt(ConfigKeys.COLOR, cfg.color) }))
    }

    override fun clientInit(event: FMLInitializationEvent) {
        logger.info("clientInit")
    }

    override fun clientPostInit(event: FMLPostInitializationEvent) {
        logger.info("clientPostInit")
    }

    override fun serverPreInit(event: FMLPreInitializationEvent) {
        logger.info("serverPreInit")
    }

    override fun serverInit(event: FMLInitializationEvent) {
        logger.info("serverInit")
    }

    override fun serverPostInit(event: FMLPostInitializationEvent) {
        logger.info("serverPostInit")
    }

    override fun registerBlocks(registry: IForgeRegistry<Block>) {
        blocks.mapSome { blocks ->
            if (blocks.enabled && blocks.enableBlock) {
                block.mapSome { block ->
                    logger.info("registerBlocks: ${block.registryName}")

                    registry.register(block)
                }
            }

            if (blocks.enabled && blocks.enableOre) {
                ore.mapSome { block ->
                    logger.info("registerBlocks: ${block.registryName}")

                    registry.register(block)
                }
            }
        }
    }

    override fun registerBlockColors(event: ColorHandlerEvent.Block) {
        val blockColors = event.blockColors

        colorHandler.mapSome { color ->
            blocks.mapSome { blocks ->
                if (blocks.enabled && blocks.enableBlock) {
                    block.mapSome { block ->
                        logger.info("registerBlockColors: ${block.registryName}")

                        blockColors.registerBlockColorHandler(color, block)
                    }
                }

                if (blocks.enabled && blocks.enableOre) {
                    ore.mapSome { block ->
                        logger.info("registerBlockColors: ${block.registryName}")

                        blockColors.registerBlockColorHandler(color, block)
                    }
                }
            }
        }
    }

    override fun registerItems(registry: IForgeRegistry<Item>) {
        blocks.mapSome { blocks ->
            if (blocks.enabled && blocks.enableBlock) {
                block.mapSome { block ->
                    logger.info("registerItems: ${block.registryName}")

                    registry.register(ItemBlock(block).setRegistryName(block.registryName))
                }
            }

            if (blocks.enabled && blocks.enableOre) {
                ore.mapSome { block ->
                    logger.info("registerItems: ${block.registryName}")

                    registry.register(ItemBlock(block).setRegistryName(block.registryName))
                }
            }
        }

        items.mapSome { items ->
            if (items.enabled && items.enableChunk) {
                chunk.mapSome { item ->
                    logger.info("registerItems: ${item.registryName}")

                    registry.register(item)
                }
            }

            if (items.enabled && items.enableDust) {
                dust.mapSome { item ->
                    logger.info("registerItems: ${item.registryName}")

                    registry.register(item)
                }
            }

            if (items.enabled && items.enableIngot) {
                ingot.mapSome { item ->
                    logger.info("registerItems: ${item.registryName}")

                    registry.register(item)
                }
            }

            if (items.enabled && items.enableNugget) {
                nugget.mapSome { item ->
                    logger.info("registerItems: ${item.registryName}")

                    registry.register(item)
                }
            }
        }

        tools.mapSome { tools ->
            if (tools.enabled && tools.enableAxe) {
                axe.mapSome { item ->
                    logger.info("registerItems: ${item.registryName}")

                    registry.register(item)
                }
            }

            if (tools.enabled && tools.enableHoe) {
                hoe.mapSome { item ->
                    logger.info("registerItems: ${item.registryName}")

                    registry.register(item)
                }
            }

            if (tools.enabled && tools.enablePickaxe) {
                pickaxe.mapSome { item ->
                    logger.info("registerItems: ${item.registryName}")

                    registry.register(item)
                }
            }

            if (tools.enabled && tools.enableShovel) {
                shovel.mapSome { item ->
                    logger.info("registerItems: ${item.registryName}")

                    registry.register(item)
                }
            }

            if (tools.enabled && tools.enableSword) {
                sword.mapSome { item ->
                    logger.info("registerItems: ${item.registryName}")

                    registry.register(item)
                }
            }
        }
    }

    override fun registerItemColors(event: ColorHandlerEvent.Item) {
        val itemColors = event.itemColors

        colorHandler.mapSome { color ->
            blocks.mapSome { blocks ->
                if (blocks.enabled && blocks.enableBlock) {
                    block.mapSome { block ->
                        logger.info("registerItemColors: ${block.registryName}")

                        itemColors.registerItemColorHandler(color, block)
                    }
                }

                if (blocks.enabled && blocks.enableOre) {
                    ore.mapSome { block ->
                        logger.info("registerItemColors: ${block.registryName}")

                        itemColors.registerItemColorHandler(color, block)
                    }
                }
            }

            items.mapSome { items ->
                if (items.enabled && items.enableChunk) {
                    chunk.mapSome { item ->
                        logger.info("registerItemColors: ${item.registryName}")

                        itemColors.registerItemColorHandler(color, item)
                    }
                }

                if (items.enabled && items.enableDust) {
                    dust.mapSome { item ->
                        logger.info("registerItemColors: ${item.registryName}")

                        itemColors.registerItemColorHandler(color, item)
                    }
                }

                if (items.enabled && items.enableIngot) {
                    ingot.mapSome { item ->
                        logger.info("registerItemColors: ${item.registryName}")

                        itemColors.registerItemColorHandler(color, item)
                    }
                }

                if (items.enabled && items.enableNugget) {
                    nugget.mapSome { item ->
                        logger.info("registerItemColors: ${item.registryName}")

                        itemColors.registerItemColorHandler(color, item)
                    }
                }
            }

            tools.mapSome { tools ->
                if (tools.enabled && tools.enableAxe) {
                    axe.mapSome { item ->
                        logger.info("registerItemColors: ${item.registryName}")

                        itemColors.registerItemColorHandler(color, item)
                    }
                }

                if (tools.enabled && tools.enableHoe) {
                    hoe.mapSome { item ->
                        logger.info("registerItemColors: ${item.registryName}")

                        itemColors.registerItemColorHandler(color, item)
                    }
                }

                if (tools.enabled && tools.enablePickaxe) {
                    pickaxe.mapSome { item ->
                        logger.info("registerItemColors: ${item.registryName}")

                        itemColors.registerItemColorHandler(color, item)
                    }
                }

                if (tools.enabled && tools.enableShovel) {
                    shovel.mapSome { item ->
                        logger.info("registerItemColors: ${item.registryName}")

                        itemColors.registerItemColorHandler(color, item)
                    }
                }

                if (tools.enabled && tools.enableSword) {
                    sword.mapSome { item ->
                        logger.info("registerItemColors: ${item.registryName}")

                        itemColors.registerItemColorHandler(color, item)
                    }
                }
            }
        }
    }

    override fun registerRenders() {
        blocks.mapSome { blocks ->
            if (blocks.enabled && blocks.enableBlock) {
                block.mapSome { block ->
                    logger.info("registerRenders: ${block.registryName}")

                    registerItemRender(block)
                }
            }

            if (blocks.enabled && blocks.enableOre) {
                ore.mapSome { block ->
                    logger.info("registerRenders: ${block.registryName}")

                    registerItemRender(block)
                }
            }
        }

        items.mapSome { items ->
            if (items.enabled && items.enableChunk) {
                chunk.mapSome { item ->
                    logger.info("registerRenders: ${item.registryName}")

                    registerItemRender(item)
                }
            }

            if (items.enabled && items.enableDust) {
                dust.mapSome { item ->
                    logger.info("registerRenders: ${item.registryName}")

                    registerItemRender(item)
                }
            }

            if (items.enabled && items.enableIngot) {
                ingot.mapSome { item ->
                    logger.info("registerRenders: ${item.registryName}")

                    registerItemRender(item)
                }
            }

            if (items.enabled && items.enableNugget) {
                nugget.mapSome { item ->
                    logger.info("registerRenders: ${item.registryName}")

                    registerItemRender(item)
                }
            }
        }

        tools.mapSome { tools ->
            if (tools.enabled && tools.enableAxe) {
                axe.mapSome { item ->
                    logger.info("registerRenders: ${item.registryName}")

                    registerItemRender(item)
                }
            }

            if (tools.enabled && tools.enableHoe) {
                hoe.mapSome { item ->
                    logger.info("registerRenders: ${item.registryName}")

                    registerItemRender(item)
                }
            }

            if (tools.enabled && tools.enablePickaxe) {
                pickaxe.mapSome { item ->
                    logger.info("registerRenders: ${item.registryName}")

                    registerItemRender(item)
                }
            }

            if (tools.enabled && tools.enableShovel) {
                shovel.mapSome { item ->
                    logger.info("registerRenders: ${item.registryName}")

                    registerItemRender(item)
                }
            }

            if (tools.enabled && tools.enableSword) {
                sword.mapSome { item ->
                    logger.info("registerRenders: ${item.registryName}")

                    registerItemRender(item)
                }
            }
        }
    }

    class ColorHandler(private val color: Int) : IBlockColor, IItemColor {
        override fun colorMultiplier(state: IBlockState, worldIn: IBlockAccess?, pos: BlockPos?, tintIndex: Int): Int {
            return if (tintIndex == 0) {
                color
            } else {
                -1
            }
        }

        override fun colorMultiplier(stack: ItemStack, tintIndex: Int): Int {
            return if (stack.item is ItemHoe || stack.item is ItemTool || stack.item is ItemSword) {
                if (tintIndex == 1) {
                    color
                } else {
                    -1
                }
            } else {
                if (tintIndex == 0) {
                    color
                } else {
                    -1
                }
            }
        }
    }

    @Suppress("MemberVisibilityCanBePrivate")
    class Builder(
        var name: String,
        var color: Int,

        var enableBlock: Boolean = false,
        var enableOre: Boolean = false,

        var enableChunk: Boolean = false,
        var enableDust: Boolean = false,
        var enableIngot: Boolean = false,
        var enableNugget: Boolean = false,

        var enableGeneration: Boolean = false,

        var blocks: Blocks,
        var items: Items,
        var generation: Generation,
        var tools: Tools
    ) {
        fun all() = apply {
            this.enableBlock().enableOre()
                .enableChunk().enableDust().enableIngot().enableNugget()
                .enableGeneration()
        }

        fun enableBlock() = apply { this.enableBlock = true }
        fun enableOre() = apply { this.enableOre = true }

        fun enableChunk() = apply { this.enableChunk = true }
        fun enableDust() = apply { this.enableDust = true }
        fun enableIngot() = apply { this.enableIngot = true }
        fun enableNugget() = apply { this.enableNugget = true }

        fun enableGeneration() = apply { this.enableGeneration = true }

        fun build(): BaseMaterial {
            this.blocks.enableBlock(this.enableBlock)
            this.blocks.enableOre(this.enableOre)

            return BaseMaterial(this)
        }
    }
}