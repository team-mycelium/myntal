package io.gitlab.teammycelium.myntal.material.common

import com.eclipsesource.json.JsonObject
import io.gitlab.teammycelium.lib.core.common.Config
import io.gitlab.teammycelium.lib.core.common.Loader
import io.gitlab.teammycelium.lib.core.common.generation.Biome
import io.gitlab.teammycelium.lib.core.common.generation.Dimension
import io.gitlab.teammycelium.lib.core.common.generation.Generation
import io.gitlab.teammycelium.lib.core.common.monads.Option
import io.gitlab.teammycelium.lib.core.common.submodule
import io.gitlab.teammycelium.myntal.core.common.config.Blocks
import io.gitlab.teammycelium.myntal.core.common.config.Items
import io.gitlab.teammycelium.myntal.core.common.config.Tools
import io.gitlab.teammycelium.myntal.material.api.IMaterial
import io.gitlab.teammycelium.myntal.material.api.IRarity
import net.minecraft.block.Block
import net.minecraft.item.Item
import net.minecraftforge.client.event.ColorHandlerEvent
import net.minecraftforge.registries.IForgeRegistry
import org.apache.logging.log4j.LogManager

class CommonRarity : IRarity, Loader<IMaterial>("common") {
    override val logger = LogManager.getLogger("Myntal/Material/Common")!!

    override var load: List<IMaterial> = listOf(
        BaseMaterial.Builder(
            "copper",
            0xd89251,
            blocks = Blocks()
                .hardness(3.0F)
                .resistance(5.0F),
            items = Items(),
            generation = Generation()
                .enabled(true)
                .kind(Generation.Type.Uniform)
                .block("minecraft:stone")
                .biome(Biome()
                    .blacklist()
                    .biomes(listOf("minecraft:desert")))
                .dimension(Dimension()
                    .whitelist()
                    .dimensions(listOf(0))),
            tools = Tools()
                .material(2, 215, 5.0F, 1.5F, 7, -3.0f)
        )
            .all()
            .build()
    )

    private var config: Option<JsonObject> = Option.None()

    override fun name(): String {
        return "common"
    }

    override fun createConfig(): JsonObject {
        logger.info("createConfig")

        val obj = JsonObject()

        load.submodule(obj)

        return obj
    }

    override fun setConfig(obj: JsonObject) {
        logger.info("setConfig")

        Config("myntal").config(load, obj)

        config = Option.Some(obj)
    }

    override fun registerBlocks(registry: IForgeRegistry<Block>) {
        logger.info("registerBlocks")

        load.forEach { it.registerBlocks(registry) }
    }

    override fun registerBlockColors(event: ColorHandlerEvent.Block) {
        logger.info("registerBlockColors")

        load.forEach { it.registerBlockColors(event) }
    }

    override fun registerItems(registry: IForgeRegistry<Item>) {
        logger.info("registerItems")

        load.forEach { it.registerItems(registry) }
    }

    override fun registerItemColors(event: ColorHandlerEvent.Item) {
        logger.info("registerItemColors")

        load.forEach { it.registerItemColors(event) }
    }

    override fun registerRenders() {
        logger.info("registerRenders")

        load.forEach { it.registerRenders() }
    }
}
