package io.gitlab.teammycelium.myntal.material.common

import com.eclipsesource.json.JsonObject
import io.gitlab.teammycelium.lib.core.api.IModule
import io.gitlab.teammycelium.lib.core.common.Config
import io.gitlab.teammycelium.lib.core.common.Loader
import io.gitlab.teammycelium.lib.core.common.monads.Option
import io.gitlab.teammycelium.lib.core.common.submodule
import io.gitlab.teammycelium.myntal.material.api.IRarity
import net.minecraft.block.Block
import net.minecraft.item.Item
import net.minecraftforge.client.event.ColorHandlerEvent
import net.minecraftforge.registries.IForgeRegistry
import org.apache.logging.log4j.LogManager

class MaterialModule : IModule, Loader<IRarity>("material") {
    override val logger = LogManager.getLogger("Myntal/Material")!!

    private var config: Option<JsonObject> = Option.None()

    override var load: List<IRarity> = listOf(
        CommonRarity()
    )

    override fun name(): String {
        return "material"
    }

    override fun createConfig(): JsonObject {
        logger.info("createConfig")

        val obj = JsonObject()

        load.submodule(obj)

        return obj
    }

    override fun setConfig(obj: JsonObject) {
        logger.info("setConfig")

        Config("myntal").config(load, obj)

        config = Option.Some(obj)
    }

    override fun registerBlocks(registry: IForgeRegistry<Block>) {
        logger.info("registerBlocks")

        load.forEach { it.registerBlocks(registry) }
    }

    override fun registerBlockColors(event: ColorHandlerEvent.Block) {
        logger.info("registerBlockColors")

        load.forEach { it.registerBlockColors(event) }
    }

    override fun registerItems(registry: IForgeRegistry<Item>) {
        logger.info("registerItems")

        load.forEach { it.registerItems(registry) }
    }

    override fun registerItemColors(event: ColorHandlerEvent.Item) {
        logger.info("registerItemColors")

        load.forEach { it.registerItemColors(event) }
    }

    override fun registerRenders() {
        logger.info("registerRenders")

        load.forEach { it.registerRenders() }
    }
}
