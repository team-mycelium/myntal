package io.gitlab.teammycelium.myntal.material.api

import io.gitlab.teammycelium.lib.core.api.IBase
import io.gitlab.teammycelium.lib.core.api.event.register.IRegister

interface IMaterial : IBase, IRegister
