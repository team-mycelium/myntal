package io.gitlab.teammycelium.myntal.material.common

import com.eclipsesource.json.JsonObject
import io.gitlab.teammycelium.lib.core.common.monads.isNone
import io.gitlab.teammycelium.lib.core.common.monads.unwrap
import io.gitlab.teammycelium.lib.core.common.serde.getIntArray
import io.gitlab.teammycelium.myntal.core.common.config.ConfigKeys
import net.minecraft.block.Block
import net.minecraft.block.state.pattern.BlockMatcher
import net.minecraft.init.Blocks
import net.minecraft.util.math.BlockPos
import net.minecraft.world.DimensionType
import net.minecraft.world.World
import net.minecraft.world.chunk.IChunkProvider
import net.minecraft.world.gen.IChunkGenerator
import net.minecraft.world.gen.feature.WorldGenMinable
import net.minecraftforge.fml.common.IWorldGenerator
import org.apache.logging.log4j.Logger
import java.util.Random

class OldGeneration(
    private val logger: Logger,
    private val obj: JsonObject,
    ore: Block
) : IWorldGenerator {
    private val endGenerator = WorldGenMinable(
        ore.defaultState,
        obj.getInt("veinSize", 6),
        BlockMatcher.forBlock(Blocks.END_STONE)
    )
    private val overworldGenerator = WorldGenMinable(
        ore.defaultState,
        obj.getInt("veinSize", 6),
        BlockMatcher.forBlock(Blocks.STONE)
    )
    private val netherGenerator = WorldGenMinable(
        ore.defaultState,
        obj.getInt("veinSize", 6),
        BlockMatcher.forBlock(Blocks.NETHERRACK)
    )

    override fun generate(
        random: Random?,
        chunkX: Int,
        chunkZ: Int,
        world: World?,
        chunkGenerator: IChunkGenerator?,
        chunkProvider: IChunkProvider?
    ) {
        if (random == null) {
            logger.error("IWorldGenerator's `random` is null, this should not happen")

            return
        }

        if (world == null) {
            logger.error("IWorldGenerator's `world` is null, this should not happen")

            return
        }

        if (chunkGenerator == null) {
            logger.error("IWorldGenerator's `chunkGenerator` is null, this should not happen")

            return
        }

        if (chunkProvider == null) {
            logger.error("IWorldGenerator's `chunkProvider` is null, this should not happen")

            return
        }

        val spawnTries = obj.getInt(ConfigKeys.GENERATION_SPAWN_TRIES, 0)
        var minHeight = obj.getInt(ConfigKeys.GENERATION_MIN_HEIGHT, 0)
        var maxHeight = obj.getInt(ConfigKeys.GENERATION_MAX_HEIGHT, 200)

        if (minHeight < 0) minHeight = 0
        if (maxHeight > 255) maxHeight = 255

        if (maxHeight < minHeight) {
            maxHeight = minHeight.also { minHeight = maxHeight }
        } else if (maxHeight == minHeight) {
            if (maxHeight < 255) {
                maxHeight++
            } else {
                minHeight--
            }
        }

        val dimensions = obj.getIntArray(ConfigKeys.GENERATION_DIMENSION_WHITELIST)

        if (dimensions.isNone()) {
            logger.error("Config's `${ConfigKeys.GENERATION_DIMENSION_WHITELIST}` is not a valid array")

            return
        }

        dimensions.unwrap().forEach {
            if (it == world.provider.dimensionType.id) {
                generator(
                    random,
                    chunkX,
                    chunkZ,
                    world,
                    when (world.provider.dimensionType) {
                        DimensionType.THE_END -> endGenerator
                        DimensionType.OVERWORLD -> overworldGenerator
                        DimensionType.NETHER -> netherGenerator
                        else -> {
                            logger.error("World provider `dimensionType` is null")

                            return@forEach
                        }
                    },
                    spawnTries,
                    minHeight,
                    maxHeight
                )
            }
        }
    }

    private fun generator(
        random: Random,
        chunkX: Int,
        chunkZ: Int,
        world: World,
        generator: WorldGenMinable,
        spawnTries: Int,
        minHeight: Int,
        maxHeight: Int
    ) {

        val chunkPosAsBlockPos = BlockPos(chunkX shl 4, 0, chunkZ shl 4)
        val heightDiff = maxHeight - minHeight + 1

        for (i in 0 until spawnTries) {
            generator.generate(
                world,
                random,
                chunkPosAsBlockPos.add(
                    random.nextInt(16),
                    minHeight + random.nextInt(heightDiff),
                    random.nextInt(16)
                )
            )
        }
    }
}