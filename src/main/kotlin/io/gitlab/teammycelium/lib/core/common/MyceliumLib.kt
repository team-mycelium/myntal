package io.gitlab.teammycelium.lib.core.common

import io.gitlab.teammycelium.lib.core.api.IModule
import io.gitlab.teammycelium.lib.core.common.monads.mapSome
import io.gitlab.teammycelium.lib.develop.common.DevelopModule
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.common.event.FMLInitializationEvent
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent
import org.apache.logging.log4j.LogManager

@Mod(
    modid = MyceliumLib.MOD_ID,
    name = "MyceliumLib",
    version = "1.0.0.0",
    dependencies = "required-after:forge@[14.21.1.2395,)"
)
class MyceliumLib {
    companion object {
        const val MOD_ID = "mycelium_lib"

        val modules: List<IModule> = listOf(
            DevelopModule()
        )
    }

    private val logger = LogManager.getLogger("MyceliumLib")

    @Mod.EventHandler
    fun preInit(event: FMLPreInitializationEvent) {
        logger.info("preInit")

        Config(name()).load(modules).mapSome {
            logger.error("Issue loading `${name()}.json`", it)
        }

        modules.forEach { it.preInit(event) }
    }

    @Mod.EventHandler
    fun init(event: FMLInitializationEvent) {
        logger.info("init")

        modules.forEach { it.init(event) }
    }

    @Mod.EventHandler
    fun postInit(event: FMLPostInitializationEvent) {
        logger.info("postInit")

        modules.forEach { it.postInit(event) }
    }

    fun name(): String {
        return "mycelium-lib"
    }
}