@file:Suppress("unused")

package io.gitlab.teammycelium.lib.core.common.utils

private const val mod = 167.toChar()

val OBFUSCATED = mod + "k"

fun String.obfuscated(): String {
    return "$OBFUSCATED$this".reset()
}

val BOLD = mod + "l"

fun String.bold(): String {
    return "$BOLD$this".reset()
}

val STRIKE_THROUGH = mod + "m"

fun String.strikeThrough(): String {
    return "$STRIKE_THROUGH$this".reset()
}

val UNDERLINE = mod + "n"

fun String.underline(): String {
    return "$UNDERLINE$this".reset()
}

val ITALIC = mod + "o"

fun String.italic(): String {
    return "$ITALIC$this".reset()
}

val RESET = mod + "r"

fun String.reset(): String {
    return "$this$RESET"
}

val BLACK = mod + "0"

fun String.black(): String {
    return "$BLACK$this".reset()
}

val DARK_BLUE = mod + "1"

fun String.darkBlue(): String {
    return "$DARK_BLUE$this".reset()
}

val DARK_GREEN = mod + "2"

fun String.darkGreen(): String {
    return "$DARK_GREEN$this".reset()
}

val DARK_AQUA = mod + "3"

fun String.darkAqua(): String {
    return "$DARK_AQUA$this".reset()
}

val DARK_RED = mod + "4"

fun String.darkRed(): String {
    return "$DARK_RED$this".reset()
}

val DARK_PURPLE = mod + "5"

fun String.darkPurple(): String {
    return "$DARK_PURPLE$this".reset()
}

val GOLD = mod + "6"

fun String.gold(): String {
    return "$GOLD$this".reset()
}

val GRAY = mod + "7"

fun String.gray(): String {
    return "$GRAY$this".reset()
}

val DARK_GRAY = mod + "8"

fun String.darkGray(): String {
    return "$DARK_GRAY$this".reset()
}

val BLUE = mod + "9"

fun String.blue(): String {
    return "$BLUE$this".reset()
}

val GREEN = mod + "a"

fun String.green(): String {
    return "$GREEN$this".reset()
}

val AQUA = mod + "b"

fun String.aqua(): String {
    return "$AQUA$this".reset()
}

val RED = mod + "c"

fun String.red(): String {
    return "$RED$this".reset()
}

val LIGHT_PURPLE = mod + "d"

fun String.lightPurple(): String {
    return "$LIGHT_PURPLE$this".reset()
}

val YELLOW = mod + "e"

fun String.yellow(): String {
    return "$YELLOW$this".reset()
}

val WHITE = mod + "f"

fun String.white(): String {
    return "$WHITE$this".reset()
}
