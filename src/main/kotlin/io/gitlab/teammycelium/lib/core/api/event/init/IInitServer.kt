package io.gitlab.teammycelium.lib.core.api.event.init

import net.minecraftforge.fml.common.event.FMLInitializationEvent
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent

interface IInitServer {
    fun serverPreInit(event: FMLPreInitializationEvent)
    fun serverInit(event: FMLInitializationEvent)
    fun serverPostInit(event: FMLPostInitializationEvent)
}