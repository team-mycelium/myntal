package io.gitlab.teammycelium.lib.core.common.utils

import io.gitlab.teammycelium.lib.core.common.localize
import net.minecraft.block.Block
import net.minecraft.util.ResourceLocation
import net.minecraftforge.fml.common.Loader

/**
 * Set the unlocalized name and registry name of the block at the same time.
 *
 * @param name The name to give the [Block]
 * @param modId If you want explicitly set the mod id. Otherwise it
 *                gets it from the current mod container
 * @param T Any kind of Block
 *
 * @example
 * ```kotlin
 * val testItem = Block(Material.ROCK).setName(BLOCK_NAME).setCreativeTab(CreativeTabs.BLOCKS)
 * ```
 *
 * @since 1.1
 */
fun <T : Block> T.setName(name: String, modId: String = Loader.instance().activeModContainer()?.modId ?: ""): T {
    unlocalizedName = name
    registryName = ResourceLocation(modId, name)
    return this
}

/**
 * Get the localized name of the block.em
 *
 * @since 1.1
 */
val Block.name: String get() = "$unlocalizedName.name".localize()
