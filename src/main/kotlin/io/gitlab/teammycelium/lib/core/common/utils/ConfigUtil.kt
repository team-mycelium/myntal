package io.gitlab.teammycelium.lib.core.common.utils

import net.minecraftforge.common.config.Configuration
import net.minecraftforge.common.config.Property

/**
 * Setup a configuration property based on a [Enum].
 *
 * @param category The category the property belongs.
 * @param key The key used for the property.
 * @param defaultValue The default enum value.
 * @param comment The comment for the property.
 * @param T The enum type.
 *
 * @since 1.3
 */
inline operator fun <reified T : Enum<T>> Configuration.get(
    category: String,
    key: String,
    defaultValue: T,
    comment: String
): Property =
    this.get(category, key, defaultValue.name, comment, T::class.java.enumConstants.map(Enum<T>::name).toTypedArray())

/**
 * If the current value is in the enum class [T].
 *
 * @param T The enum type.
 *
 * @since 1.1
 */
inline fun <reified T : Enum<T>> Property.isEnum(): Boolean = try {
    string in T::class.java.fields.map { it.name }
} catch (e: Throwable) {
    false
}

/**
 * Get the property value as a enum class [T].
 *
 * @param T The enum type.
 *
 * @since 1.1
 */
inline fun <reified T : Enum<T>> Property.getEnum(): T =
    if (isEnum<T>()) {
        T::class.java.getField(string).get(null) as T
    } else {
        T::class.java.getField(default).get(null) as T
    }

/**
 * Set the value of the property form a enum value.
 *
 * @param value The enum value.
 * @param T The enum type.
 *
 * @since 1.1
 */
fun <T : Enum<T>> Property.setEnum(value: T) = set(value.name)
