package io.gitlab.teammycelium.lib.core.common.utils

import io.gitlab.teammycelium.lib.core.common.localize
import net.minecraft.item.Item
import net.minecraft.util.ResourceLocation
import net.minecraftforge.fml.common.Loader

/**
 * Set the unlocalized name and registry name of the item at the same time.
 *
 * @param name The name to give the [Item]
 * @param modId If you want explicitly set the mod id. Otherwise it
 *                gets it from the current mod container
 * @param T Any kind of Item
 *
 * @example
 * ```kotlin
 * val testItem = Item().setName(ITEM_NAME).setCreativeTab(CreativeTabs.MISC)
 * ```
 *
 * @since 1.1
 */
fun <T : Item> T.setName(name: String, modId: String = Loader.instance().activeModContainer()?.modId ?: ""): T {
    unlocalizedName = name
    registryName = ResourceLocation(modId, name)
    return this
}

/**
 * Get the localized name of the item.
 *
 * @since 1.1
 */
val Item.name: String get() = "$unlocalizedName.name".localize()
