package io.gitlab.teammycelium.lib.core.common.items.tools

import io.gitlab.teammycelium.lib.core.common.localize
import net.minecraft.creativetab.CreativeTabs
import net.minecraft.item.Item
import net.minecraft.item.ItemHoe
import net.minecraft.util.ResourceLocation
import net.minecraftforge.fml.common.Loader

class KHoe(material: Item.ToolMaterial) : ItemHoe(material) {
    val name: String get() = "$unlocalizedName.name".localize()

    fun name(name: String, modId: String = Loader.instance().activeModContainer()?.modId ?: ""): KHoe {
        unlocalizedName = "$modId.$name"
        registryName = ResourceLocation(modId, name)

        return this
    }

    fun creativeTab(tab: CreativeTabs): KHoe {
        creativeTab = tab

        return this
    }
}