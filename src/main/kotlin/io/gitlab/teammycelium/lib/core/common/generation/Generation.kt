package io.gitlab.teammycelium.lib.core.common.generation

import com.eclipsesource.json.JsonObject
import io.gitlab.teammycelium.lib.core.common.Config
import io.gitlab.teammycelium.lib.core.common.monads.Option
import io.gitlab.teammycelium.lib.core.common.monads.andThen
import io.gitlab.teammycelium.lib.core.common.monads.flatMap
import io.gitlab.teammycelium.lib.core.common.monads.map
import io.gitlab.teammycelium.lib.core.common.serde.obj
import io.gitlab.teammycelium.lib.core.common.serde.tryBoolean
import io.gitlab.teammycelium.lib.core.common.serde.tryObject
import io.gitlab.teammycelium.lib.core.common.serde.tryString

class Generation {
    var enabled: Boolean = true

    fun enabled(value: Boolean): Generation {
        enabled = value

        return this
    }

    enum class Type {
        Uniform,
        Gaussian;
    }

    var type = Type.Uniform

    fun kind(list: Type): Generation {
        type = list

        return this
    }

    lateinit var biome: Biome

    fun biome(builder: Biome): Generation {
        biome = builder

        return this
    }

    lateinit var dimension: Dimension

    fun dimension(builder: Dimension): Generation {
        dimension = builder

        return this
    }

    lateinit var block: String

    fun block(name: String): Generation {
        block = name

        return this
    }

    companion object {
        fun read(obj: JsonObject): Option<Generation> {
            return obj.get(Config.Keys.ENABLED)
                .tryBoolean()
                .andThen { enabled ->
                    obj.get(Config.Keys.KIND)
                        .tryString()
                        .andThen { kind ->
                            obj.get(Config.Keys.PARAMETERS)
                                .tryObject()
                                .andThen { parameters ->
                                    parameters.get(Config.Keys.BLOCK)
                                        .tryString()
                                        .andThen { block ->
                                            parameters.get(Config.Keys.BIOME)
                                                .tryObject()
                                                .flatMap(Biome.Companion::read)
                                                .andThen { biome ->
                                                    parameters.get(Config.Keys.DIMENSION)
                                                        .tryObject()
                                                        .flatMap(Dimension.Companion::read)
                                                        .map { dimension ->
                                                            Generation()
                                                                .enabled(enabled)
                                                                .kind(when (kind.toLowerCase()) {
                                                                    "uniform" -> Type.Uniform
                                                                    "gaussian" -> Type.Gaussian
                                                                    else -> return Option.None()
                                                                })
                                                                .biome(biome)
                                                                .dimension(dimension)
                                                                .block(block)
                                                        }
                                                }
                                        }
                                }
                        }
                }
        }
    }

    fun write(): JsonObject {
        return obj {
            Config.Keys.ENABLED to enabled
            Config.Keys.KIND to when (type) {
                Type.Uniform -> "uniform"
                Type.Gaussian -> "gaussian"
            }
            Config.Keys.PARAMETERS to obj {
                Config.Keys.BIOME to biome.write()
                Config.Keys.DIMENSION to dimension.write()
                Config.Keys.BLOCK to block
            }
        }
    }
}