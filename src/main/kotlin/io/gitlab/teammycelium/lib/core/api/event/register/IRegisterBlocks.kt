package io.gitlab.teammycelium.lib.core.api.event.register

import net.minecraft.block.Block
import net.minecraftforge.client.event.ColorHandlerEvent
import net.minecraftforge.registries.IForgeRegistry

interface IRegisterBlocks {
    fun registerBlocks(registry: IForgeRegistry<Block>)
    fun registerBlockColors(event: ColorHandlerEvent.Block)
}
