package io.gitlab.teammycelium.lib.core.common.items.tools

import io.gitlab.teammycelium.lib.core.common.localize
import net.minecraft.creativetab.CreativeTabs
import net.minecraft.item.ItemSpade
import net.minecraft.util.ResourceLocation
import net.minecraftforge.fml.common.Loader

class KShovel(material: ToolMaterial) : ItemSpade(material) {
    val name: String get() = "$unlocalizedName.name".localize()

    fun name(name: String, modId: String = Loader.instance().activeModContainer()?.modId ?: ""): KShovel {
        unlocalizedName = "$modId.$name"
        registryName = ResourceLocation(modId, name)

        return this
    }

    fun creativeTab(tab: CreativeTabs): KShovel {
        creativeTab = tab

        return this
    }
}