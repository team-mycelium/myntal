package io.gitlab.teammycelium.lib.core.common.utils

import net.minecraft.world.World

/**
 * If the world is on the server.
 *
 * @since 1.2
 */
val World.isServer: Boolean get() = !isRemote

/**
 * If the world is on the client.
 *
 * @since 1.2
 */
val World.isClient: Boolean get() = isRemote
