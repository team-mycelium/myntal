package io.gitlab.teammycelium.lib.core.common.blocks

import io.gitlab.teammycelium.lib.core.common.localize
import io.gitlab.teammycelium.lib.core.common.monads.Option
import io.gitlab.teammycelium.lib.core.common.monads.match
import net.minecraft.block.Block
import net.minecraft.block.material.MapColor
import net.minecraft.block.material.Material
import net.minecraft.block.state.IBlockState
import net.minecraft.creativetab.CreativeTabs
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.util.BlockRenderLayer
import net.minecraft.util.EnumFacing
import net.minecraft.util.EnumHand
import net.minecraft.util.ResourceLocation
import net.minecraft.util.math.BlockPos
import net.minecraft.world.World
import net.minecraftforge.fml.common.Loader

typealias BlockActivatedHandler = (
    worldIn: World,
    pos: BlockPos,
    state: IBlockState,
    playerIn: EntityPlayer,
    hand: EnumHand,
    facing: EnumFacing,
    hitX: Float,
    hitY: Float,
    hitZ: Float
) -> Boolean

class KBlock : Block {
    private var activatedHandler: Option<BlockActivatedHandler> = Option.None()
    private var kblockLayer: BlockRenderLayer = BlockRenderLayer.SOLID

    val name: String get() = "$unlocalizedName.name".localize()

    constructor(material: Material) : this(material, material.materialMapColor)
    constructor(material: Material, mapColor: MapColor) : super(material, mapColor)

    fun name(name: String, modId: String = Loader.instance().activeModContainer()?.modId ?: ""): KBlock {
        unlocalizedName = "$modId.$name"
        registryName = ResourceLocation(modId, name)

        return this
    }

    fun hardness(h: Float): KBlock {
        blockHardness = h

        return this
    }

    fun resistance(r: Float): KBlock {
        blockResistance = r

        return this
    }

    fun creativeTab(tab: CreativeTabs): KBlock {
        setCreativeTab(tab)

        return this
    }

    fun blockLayer(layer: BlockRenderLayer): KBlock {
        kblockLayer = layer

        return this
    }

    fun activatedHandler(handlerBlock: BlockActivatedHandler): KBlock {
        activatedHandler = Option.Some(handlerBlock)

        return this
    }

    override fun getBlockLayer(): BlockRenderLayer {
        return kblockLayer
    }

    override fun onBlockActivated(
        worldIn: World,
        pos: BlockPos,
        state: IBlockState,
        playerIn: EntityPlayer,
        hand: EnumHand,
        facing: EnumFacing,
        hitX: Float,
        hitY: Float,
        hitZ: Float
    ): Boolean {
        return activatedHandler.match(
            some = { it(worldIn, pos, state, playerIn, hand, facing, hitX, hitY, hitZ) },
            none = { false }
        )
    }
}
