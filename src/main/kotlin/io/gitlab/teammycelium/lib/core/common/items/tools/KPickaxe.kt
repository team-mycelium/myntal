package io.gitlab.teammycelium.lib.core.common.items.tools

import io.gitlab.teammycelium.lib.core.common.localize
import net.minecraft.creativetab.CreativeTabs
import net.minecraft.item.ItemPickaxe
import net.minecraft.util.ResourceLocation
import net.minecraftforge.fml.common.Loader

class KPickaxe(material: ToolMaterial) : ItemPickaxe(material) {
    val name: String get() = "$unlocalizedName.name".localize()

    fun name(name: String, modId: String = Loader.instance().activeModContainer()?.modId ?: ""): KPickaxe {
        unlocalizedName = "$modId.$name"
        registryName = ResourceLocation(modId, name)

        return this
    }

    fun creativeTab(tab: CreativeTabs): KPickaxe {
        creativeTab = tab

        return this
    }
}