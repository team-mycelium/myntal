package io.gitlab.teammycelium.lib.core.common.generation

import io.gitlab.teammycelium.lib.core.common.monads.Option
import io.gitlab.teammycelium.lib.core.common.monads.andThen
import io.gitlab.teammycelium.lib.core.common.monads.map
import io.gitlab.teammycelium.lib.core.common.serde.andThen
import com.eclipsesource.json.JsonObject
import io.gitlab.teammycelium.lib.core.common.serde.obj
import io.gitlab.teammycelium.lib.core.common.serde.tryIntArray
import io.gitlab.teammycelium.lib.core.common.serde.tryObject
import io.gitlab.teammycelium.lib.core.common.serde.tryString

class Dimension {
    enum class Type {
        Blacklist,
        Whitelist;
    }

    var type = Type.Blacklist

    fun kind(list: Type): Dimension {
        type = list

        return this
    }

    fun blacklist(): Dimension {
        type = Type.Blacklist

        return this
    }

    fun whitelist(): Dimension {
        type = Type.Whitelist

        return this
    }

    var list: List<Int> = listOf()

    fun dimensions(dimensions: List<Int>): Dimension {
        list = dimensions

        return this
    }

    companion object {
        fun read(obj: JsonObject): Option<Dimension> {
            return obj.get("kind")
                .tryString()
                .andThen { kind ->
                    kind.equals("blacklist", true)
                        .not()
                        .andThen {
                            kind.equals("whitelist", true)
                                .not()
                                .andThen {
                                    obj.get("parameters")
                                        .tryObject()
                                        .andThen { parameters ->
                                            parameters.get("list")
                                                .tryIntArray()
                                                .map { list ->
                                                    Dimension()
                                                        .kind(when (kind.toLowerCase()) {
                                                            "blacklist" -> Type.Blacklist
                                                            "whitelist" -> Type.Whitelist
                                                            else -> return Option.None()
                                                        })
                                                        .dimensions(list)
                                                }
                                        }
                                }
                        }
                }
        }
    }

    fun write(): JsonObject {
        return obj {
            "kind" to when (type) {
                Type.Blacklist -> "blacklist"
                Type.Whitelist -> "whitelist"
            }
            "parameters" to obj {
                "list" to list
            }
        }
    }
}