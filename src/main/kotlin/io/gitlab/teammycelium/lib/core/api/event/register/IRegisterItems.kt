package io.gitlab.teammycelium.lib.core.api.event.register

import net.minecraft.item.Item
import net.minecraftforge.client.event.ColorHandlerEvent
import net.minecraftforge.registries.IForgeRegistry

interface IRegisterItems {
    fun registerItems(registry: IForgeRegistry<Item>)
    fun registerItemColors(event: ColorHandlerEvent.Item)
}
