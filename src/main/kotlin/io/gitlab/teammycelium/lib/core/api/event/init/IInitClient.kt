package io.gitlab.teammycelium.lib.core.api.event.init

import net.minecraftforge.fml.common.event.FMLInitializationEvent
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent

interface IInitClient {
    fun clientPreInit(event: FMLPreInitializationEvent)
    fun clientInit(event: FMLInitializationEvent)
    fun clientPostInit(event: FMLPostInitializationEvent)
}