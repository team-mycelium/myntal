package io.gitlab.teammycelium.lib.core.client

import io.gitlab.teammycelium.lib.core.common.logger
import net.minecraft.block.Block
import net.minecraft.client.renderer.block.model.ModelResourceLocation
import net.minecraft.item.Item
import net.minecraftforge.client.model.ModelLoader

fun registerItemRender(block: Block) {
    registerItemRender(Item.getItemFromBlock(block))
}

fun registerItemRender(item: Item) {
    val name = item.registryName

    if (name != null) {
        ModelLoader.registerItemVariants(item, name)
        ModelLoader.setCustomModelResourceLocation(item, 0, ModelResourceLocation(name, "inventory"))
    } else {
        logger.warn("Item's `registryName` is null: ${item.unlocalizedName}")
    }
}
