package io.gitlab.teammycelium.lib.core.common.generation

import io.gitlab.teammycelium.lib.core.common.monads.Option
import io.gitlab.teammycelium.lib.core.common.monads.andThen
import io.gitlab.teammycelium.lib.core.common.monads.map
import io.gitlab.teammycelium.lib.core.common.serde.andThen
import com.eclipsesource.json.JsonObject
import io.gitlab.teammycelium.lib.core.common.serde.obj
import io.gitlab.teammycelium.lib.core.common.serde.tryObject
import io.gitlab.teammycelium.lib.core.common.serde.tryString
import io.gitlab.teammycelium.lib.core.common.serde.tryStringArray

class Biome {
    enum class Type {
        Blacklist,
        Whitelist;
    }

    var type = Type.Blacklist

    fun kind(list: Type): Biome {
        type = list

        return this
    }

    fun blacklist(): Biome {
        type = Type.Blacklist

        return this
    }

    fun whitelist(): Biome {
        type = Type.Whitelist

        return this
    }

    var list: List<String> = listOf()

    fun biomes(biomes: List<String>): Biome {
        list = biomes

        return this
    }

    companion object {
        fun read(obj: JsonObject): Option<Biome> {
            return obj.get("kind")
                .tryString()
                .andThen { kind ->
                    kind.equals("blacklist", true)
                        .not()
                        .andThen {
                            kind.equals("whitelist", true)
                                .not()
                                .andThen {
                                    obj.get("parameters")
                                        .tryObject()
                                        .andThen { parameters ->
                                            parameters.get("list")
                                                .tryStringArray()
                                                .map { list ->
                                                    Biome()
                                                        .kind(when (kind.toLowerCase()) {
                                                            "blacklist" -> Type.Blacklist
                                                            "whitelist" -> Type.Whitelist
                                                            else -> return Option.None()
                                                        })
                                                        .biomes(list)
                                                }
                                        }
                                }
                        }
                }
        }
    }

    fun write(): JsonObject {
        return obj {
            "kind" to when (type) {
                Type.Blacklist -> "blacklist"
                Type.Whitelist -> "whitelist"
            }
            "parameters" to obj {
                "list" to list
            }
        }
    }
}