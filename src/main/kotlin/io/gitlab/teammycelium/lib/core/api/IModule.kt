package io.gitlab.teammycelium.lib.core.api

import io.gitlab.teammycelium.lib.core.api.event.register.IRegister

interface IModule: IBase, IRegister
