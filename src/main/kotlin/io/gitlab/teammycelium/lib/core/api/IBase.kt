package io.gitlab.teammycelium.lib.core.api

import io.gitlab.teammycelium.lib.core.api.event.init.IInit

interface IBase: IInit, IConfig
