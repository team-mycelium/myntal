package io.gitlab.teammycelium.lib.core.common

import io.gitlab.teammycelium.lib.core.api.event.init.IInitCommon
import net.minecraftforge.fml.common.event.FMLInitializationEvent
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent
import org.apache.logging.log4j.Logger

abstract class Loader<T: IInitCommon>(private val name: String) {
    abstract val logger: Logger

    abstract var load: List<T>

    fun preInit(event: FMLPreInitializationEvent) {
        logger.info("preInit")

        load.forEach { it.preInit(event) }
    }

    fun init(event: FMLInitializationEvent) {
        logger.info("init")

        load.forEach { it.init(event) }
    }

    fun postInit(event: FMLPostInitializationEvent) {
        logger.info("postInit")

        load.forEach { it.postInit(event) }
    }

    fun clientPreInit(event: FMLPreInitializationEvent) {
        logger.info("clientPreInit")

        load.forEach { it.clientPreInit(event) }
    }

    fun clientInit(event: FMLInitializationEvent) {
        logger.info("clientInit")

        load.forEach { it.clientInit(event) }
    }

    fun clientPostInit(event: FMLPostInitializationEvent) {
        logger.info("clientPostInit")

        load.forEach { it.clientPostInit(event) }
    }

    fun serverPreInit(event: FMLPreInitializationEvent) {
        logger.info("serverPreInit")

        load.forEach { it.serverPreInit(event) }
    }

    fun serverInit(event: FMLInitializationEvent) {
        logger.info("serverInit")

        load.forEach { it.serverInit(event) }
    }

    fun serverPostInit(event: FMLPostInitializationEvent) {
        logger.info("serverPostInit")

        load.forEach { it.serverPostInit(event) }
    }
}