package io.gitlab.teammycelium.lib.core.common.serde

import com.eclipsesource.json.Json
import com.eclipsesource.json.JsonArray
import com.eclipsesource.json.JsonObject
import com.eclipsesource.json.JsonValue
import io.gitlab.teammycelium.lib.core.common.monads.Option
import io.gitlab.teammycelium.lib.core.common.monads.andThen
import io.gitlab.teammycelium.lib.core.common.monads.asSome
import io.gitlab.teammycelium.lib.core.common.monads.isNone
import io.gitlab.teammycelium.lib.core.common.monads.unwrap

fun arr(vararg item: Any?): JsonArray {
    val array = JsonArray()

    item.forEach {
        when (it) {
            is Boolean -> array.add(it)
            is String -> array.add(it)
            is Double -> array.add(it)
            is Float -> array.add(it)
            is Int -> array.add(it)
            is Long -> array.add(it)
            is JsonValue -> array.add(it)
            null -> array.add(Json.NULL)
            else -> array.add(it.toString())
        }
    }

    return array
}

fun obj(init: JsonObjectBuilder.() -> Unit): JsonObject {
    val builder = JsonObjectBuilder()

    builder.init()

    return builder.build()
}

class JsonObjectBuilder internal constructor() {
    private val obj = JsonObject()

    infix fun String.to(value: Any?) {
        when (value) {
            is Boolean -> obj.add(this, value)
            is String -> obj.add(this, value)
            is Double -> obj.add(this, value)
            is Float -> obj.add(this, value)
            is Int -> obj.add(this, value)
            is Long -> obj.add(this, value)
            is List<*> -> obj.add(this, value.intoJsonArray())
            is JsonValue -> obj.add(this, value)
            null -> obj.add(this, Json.NULL)
            else -> obj.add(this, value.toString())
        }
    }

    internal fun build(): JsonObject {
        return obj
    }
}

fun List<*>.intoJsonArray(): JsonArray {
    val arr = JsonArray()

    forEach {
        when (it) {
            is Boolean -> arr.add(it)
            is String -> arr.add(it)
            is Double -> arr.add(it)
            is Float -> arr.add(it)
            is Int -> arr.add(it)
            is Long -> arr.add(it)
            is List<*> -> arr.add(it.intoJsonArray())
            is JsonValue -> arr.add(it)
            null -> arr.add(Json.NULL)
            else -> arr.add(it.toString())
        }
    }

    return arr
}

inline fun <U> Boolean.andThen(action: (Boolean) -> Option<U>): Option<U> = if (this) action(this) else Option.None()

fun JsonObject.getArray(key: String): Option<JsonArray> {
    val arr = get(key) ?: return Option.None()

    if (!arr.isArray) {
        return Option.None()
    }

    return Option.Some(arr.asArray())
}

fun JsonObject.getIntArray(key: String): Option<List<Int>> {
    val arr = getArray(key)

    if (arr.isNone()) {
        return Option.None()
    }

    val list = ArrayList<Int>()

    arr.unwrap().forEach { i ->
        if (i == null) {
            return Option.None()
        }

        if (!i.isNumber) {
            return Option.None()
        }

        list.add(i.asInt())
    }

    return Option.Some(list)
}

fun JsonArray?.tryIntArray(): Option<List<Int>> {
    if (this == null) {
        return  Option.None()
    }

    val arr = mutableListOf<Int>()

    forEach {
        if (!it.isNumber) {
            return Option.None()
        }

        arr.add(it.asInt())
    }

    return arr.asSome()
}

fun JsonArray?.tryStringArray(): Option<List<String>> {
    if (this == null) {
        return  Option.None()
    }

    val arr = mutableListOf<String>()

    forEach {
        if (!it.isString) {
            return Option.None()
        }

        arr.add(it.asString()!!)
    }

    return arr.asSome()
}

fun JsonValue?.tryArray(): Option<JsonArray> {
    return if (this == null) {
        Option.None()
    } else {
        if (this.isArray) {
            this.asArray()!!.asSome()
        } else {
            Option.None()
        }
    }
}

fun JsonValue?.tryIntArray(): Option<List<Int>> {
    return this.tryArray()
        .andThen(JsonArray::tryIntArray)
}

fun JsonValue?.tryStringArray(): Option<List<String>> {
    return this.tryArray()
        .andThen(JsonArray::tryStringArray)
}

fun JsonValue?.tryBoolean(): Option<Boolean> {
    return if (this == null) {
        Option.None()
    } else {
        if (this.isBoolean) {
            this.asBoolean().asSome()
        } else {
            Option.None()
        }
    }
}

fun JsonValue?.tryFloat(): Option<Float> {
    return if (this == null) {
        Option.None()
    } else {
        if (this.isNumber) {
            this.asFloat().asSome()
        } else {
            Option.None()
        }
    }
}

fun JsonValue?.tryInt(): Option<Int> {
    return if (this == null) {
        Option.None()
    } else {
        if (this.isNumber) {
            this.asInt().asSome()
        } else {
            Option.None()
        }
    }
}

fun JsonValue?.tryString(): Option<String> {
    return if (this == null) {
        Option.None()
    } else {
        if (this.isString) {
            this.asString()!!.asSome()
        } else {
            Option.None()
        }
    }
}

fun JsonValue?.tryObject(): Option<JsonObject> {
    return if (this == null) {
        Option.None()
    } else {
        if (this.isObject) {
            this.asObject()!!.asSome()
        } else {
            Option.None()
        }
    }
}