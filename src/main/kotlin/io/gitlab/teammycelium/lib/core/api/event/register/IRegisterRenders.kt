package io.gitlab.teammycelium.lib.core.api.event.register

interface IRegisterRenders {
    fun registerRenders()
}
