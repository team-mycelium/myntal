package io.gitlab.teammycelium.lib.core.common

import com.eclipsesource.json.Json
import com.eclipsesource.json.JsonObject
import com.eclipsesource.json.PrettyPrint
import io.gitlab.teammycelium.lib.core.api.IConfig
import io.gitlab.teammycelium.lib.core.common.monads.Option
import io.gitlab.teammycelium.lib.core.common.monads.error
import io.gitlab.teammycelium.lib.core.common.monads.isError
import io.gitlab.teammycelium.lib.core.common.monads.mapOr
import io.gitlab.teammycelium.lib.core.common.monads.mapSome
import io.gitlab.teammycelium.lib.core.common.monads.matchVoid
import io.gitlab.teammycelium.lib.core.common.monads.option
import io.gitlab.teammycelium.lib.core.common.monads.resultOf
import io.gitlab.teammycelium.lib.core.common.monads.unwrap
import net.minecraftforge.fml.common.Loader
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.InputStreamReader
import java.io.OutputStreamWriter

class Config(private val name: String) {
    object Keys {
        const val DEBUG = "debug"

        const val ENABLED = "enabled"

        const val PARAMETERS = "parameters"

        const val BLOCK = "block"
        const val BIOME = "biome"
        const val DIMENSION = "dimension"
        const val KIND = "kind"

        const val GENERATION = "generation"
        const val GENERATION_DIMENSION_WHITELIST = "dimensionWhitelist"
        const val GENERATION_MIN_HEIGHT = "minHeight"
        const val GENERATION_MAX_HEIGHT = "maxHeight"
        const val GENERATION_SPAWN_TRIES = "spawnTries"
        const val GENERATION_VEIN_SIZE = "veinSize"
    }

    companion object {
        fun loop(obj: JsonObject, modules: List<IConfig>) {
            modules.forEach {
                val moduleObject = JsonObject()

                moduleObject.add(Keys.ENABLED, true)

                moduleObject.add(Keys.PARAMETERS, it.createConfig())

                obj.add(it.name(), moduleObject)

                it.setConfig(it.createConfig())
            }
        }
    }

    private fun write(modules: List<IConfig>, configFile: File) {
        val obj = JsonObject()

        loop(obj, modules)

        val writer = OutputStreamWriter(FileOutputStream(configFile))

        obj.writeTo(writer, PrettyPrint.indentWithSpaces(4))

        writer.flush()
    }

    fun config(modules: List<IConfig>, obj: JsonObject) {
        obj.forEach { pair ->
            if (modules.any { it.name() == pair.name }) {
                if (!pair.value.isObject) {
                    logger.error("${pair.name} is not an object")

                    return@forEach
                }

                configEnabled(modules, pair, pair.value.asObject())
            } else {
                logger.warn("No module found with the name ${pair.name}, has the module been removed or renamed?")
            }
        }
    }

    private fun configEnabled(modules: List<IConfig>, pair: JsonObject.Member, pairObj: JsonObject) {
        pairObj.get(Keys.ENABLED).option().matchVoid(
            some = { enabledValue ->
                if (enabledValue.isBoolean) {
                    if (enabledValue.asBoolean()) {
                        configParameters(modules, pair, pairObj)
                    } else {
                        logger.info("Skipping ${pair.name} as it is disabled")
                    }
                } else {
                    logger.error("${pair.name}'s `enabled` is not an boolean")
                }
            },
            none = {
                logger.warn("${pair.name}'s `enabled` is missing")
            }
        )
    }

    private fun configParameters(modules: List<IConfig>, pair: JsonObject.Member, obj: JsonObject) {
        obj.get(Keys.PARAMETERS).option().matchVoid(
            some = { parameterValue ->
                if (parameterValue.isObject) {
                    modules.find { it.name() == pair.name }.option().mapSome {
                        it.setConfig(parameterValue.asObject())
                    }
                } else {
                    logger.error("${pair.name}'s `parameter` is not an object")
                }
            },
            none = {
                logger.warn("${pair.name}'s `parameters` is missing")
            }
        )
    }

    fun load(modules: List<IConfig>): Option<Throwable> {
        val configFolder = Loader.instance().configDir.toString() + File.separator + "team-mycelium"

        with(File(configFolder)) {
            if (!exists()) {
                mkdirs()
            }
        }

        val configPath = configFolder + File.separator + "$name.json"

        val configFile = File(configPath)

        if (!configFile.exists()) {
            logger.info("Could not find `$name.json`, creating a new one")

            write(modules, configFile)

            return Option.None()
        }

        if (!(configFile.canRead() && configFile.canWrite())) {
            return Option.Some(FileNotReadWritableException("Could not read or write to `$name.json`"))
        }

        val reader = InputStreamReader(FileInputStream(configFile))

        val json = resultOf { Json.parse(reader) }

        if (json.isError()) {
            logger.error("Could not parse `$name.json`, either its not JSON or its corrupt")
            logger.error("If no changes have been made you can try deleting it")

            return json.error()
        } else {
            val value = json.unwrap()

            if (!value.isObject) {
                logger.error("Config is not an object, something is wrong with it")

                return Option.Some(JsonSchemeException("`$name.json`'s root value must be a object"))
            }

            val obj = value.asObject()

            config(modules, obj)

            return Option.None()
        }
    }

    class JsonSchemeException(msg: String) : Exception(msg)

    class FileNotReadWritableException(msg: String) : RuntimeException(msg)
}

fun List<IConfig>.submodule(obj: JsonObject) {
    forEach {
        val materialObject = JsonObject()

        materialObject.add(Config.Keys.ENABLED, true)

        materialObject.add(Config.Keys.PARAMETERS, it.createConfig())

        obj.add(it.name(), materialObject)
    }
}

fun Option<JsonObject>.enabled(key: String): Boolean {
    return this.mapOr(false) { config ->
        return if (config.getBoolean(Config.Keys.ENABLED, true)) {
            config.getBoolean(key, true)
        } else {
            false
        }
    }
}

fun Option<JsonObject>.getObj(key: String): Option<JsonObject> {
    return this.mapOr(Option.None()) { config ->
        val value = config.get(key)

        return if (value == null) {
            Option.None()
        } else {
            if (value.isObject) {
                Option.Some(value.asObject())
            } else {
                Option.None()
            }
        }
    }
}