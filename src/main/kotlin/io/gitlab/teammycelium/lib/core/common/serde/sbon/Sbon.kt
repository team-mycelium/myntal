package io.gitlab.teammycelium.lib.core.common.serde.sbon

import java.io.Reader

class Sbon {
    companion object {
        fun parse(reader: Reader): SbonValue? {
            return null
        }
    }
}


class SbonReader {
    private val defaultBufferSize = 1024
    private val minBufferSize = 10

    private val maxNestingLevel = 1000

    private lateinit var reader: Reader
    private lateinit var buffer: CharArray

    private var bufferOffset = 0
    private var index = 0
    private var fill = 0
    private var line = 0

    private var lineOffset = 0
    private var current: Char = Char.MIN_VALUE

    private var captureStart = -1
    private var nestingLevel = 0

    fun parse(reader: Reader) {
        parse(reader, defaultBufferSize)
    }

    fun parse(reader: Reader, bufferSize: Int) {
        if (bufferSize <= 0) {
            return
        }

        this.reader = reader
        this.buffer = CharArray(bufferSize)
    }

//    fun read() {
//        if (index == fill) {
//            if (captureStart != -1) {
//                buffer.append(buffer, captureStart, fill - captureStart)
//                captureStart = 0
//            }
//
//            bufferOffset += fill
//            fill = reader.read(buffer, 0, buffer.size)
//            index = 0
//
//            if (fill == -1) {
//                current = -1
//                index++
//
//                return
//            }
//        }
//
//        if (current == '\n') {
//            line++
//            lineOffset = bufferOffset + index
//        }
//
//        current = buffer[index++]
//    }
}

class SbonWriter {}


class SbonBase {}

class SbonLiteral {}

class SbonArray {}

class SbonNumber {}

class SbonObject {}

class SbonString {}

class SbonValue {}