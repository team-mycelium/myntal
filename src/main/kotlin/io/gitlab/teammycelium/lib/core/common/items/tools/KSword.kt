package io.gitlab.teammycelium.lib.core.common.items.tools

import io.gitlab.teammycelium.lib.core.common.localize
import net.minecraft.creativetab.CreativeTabs
import net.minecraft.item.Item
import net.minecraft.item.ItemSword
import net.minecraft.util.ResourceLocation
import net.minecraftforge.fml.common.Loader

class KSword(material: Item.ToolMaterial) : ItemSword(material) {
    val name: String get() = "$unlocalizedName.name".localize()

    fun name(name: String, modId: String = Loader.instance().activeModContainer()?.modId ?: ""): KSword {
        unlocalizedName = "$modId.$name"
        registryName = ResourceLocation(modId, name)

        return this
    }

    fun creativeTab(tab: CreativeTabs): KSword {
        creativeTab = tab

        return this
    }
}