@file:Suppress("UNUSED_PARAMETER", "unused", "MemberVisibilityCanPrivate", "LeakingThis", "RedundantUnitExpression")
@file:JvmName("Utils")

// Taken from ocpu's Boxlin
// https://github.com/ocpu/Boxlin/blob/7abf54299e79ba7eb274d1e56d078f83c4111016/src/main/java/io/opencubes/boxlin/utils.kt

package io.gitlab.teammycelium.lib.core.common

import net.minecraft.client.gui.GuiScreen
import net.minecraft.creativetab.CreativeTabs
import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import net.minecraft.util.EnumFacing
import net.minecraft.util.NonNullList
import net.minecraftforge.common.capabilities.Capability
import net.minecraftforge.common.capabilities.ICapabilityProvider
import net.minecraftforge.common.config.ConfigElement
import net.minecraftforge.common.config.Configuration
import net.minecraftforge.fml.client.IModGuiFactory
import net.minecraftforge.fml.client.config.GuiConfig
import net.minecraftforge.fml.client.config.IConfigElement
import net.minecraftforge.fml.common.FMLCommonHandler
import net.minecraftforge.fml.common.Loader
import net.minecraftforge.fml.common.SidedProxy
import net.minecraftforge.fml.relauncher.Side
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import java.util.Optional
import java.util.function.Supplier
import kotlin.reflect.KClass

/**
 * Get a logger with your mod name.
 *
 * @since 1.1
 */
val logger: Logger get() = LogManager.getLogger(Loader.instance().activeModContainer()?.name)

/**
 * Get all variants of a item in a [NonNullList].
 *
 * @since 1.1
 */
val Item.variants: NonNullList<ItemStack>
    get() {
        val list = NonNullList.create<ItemStack>()
        val cTab: CreativeTabs = creativeTab ?: CreativeTabs.SEARCH
        getSubItems(cTab, list)
        return list
    }

/**
 * Provides a easy way to create a configuration GUI screen from a [Configuration].
 *
 * @param parent The parent screen provided by [IModGuiFactory.createConfigGui].
 * @param config The [Configuration] to use to generate the GUI screen.
 * @param modId Your mod ID.
 * @param title The title of the configuration GUI.
 * @return The configuration GUI screen.
 *
 * @example
 * ```kotlin
 * val config = Configuration(FILE)
 * // Configs...
 * val screen = getGuiConfig(PARENT, config, MOD_ID, "My configuration")
 * ```
 *
 * @since 1.3
 */
fun getGuiConfigScreen(parent: GuiScreen, config: Configuration, modId: String, title: String): GuiConfig {
    var allRequiresWorldRestart = true
    var allRequiresMCRestart = true
    val categories = mutableListOf<ConfigElement>()

    for (name in config.categoryNames) {
        val category = config.getCategory(name)
        categories.add(ConfigElement(category))

        req@ for (item in category.keys) {
            val prop = category[item]

            if (!allRequiresMCRestart && !allRequiresWorldRestart) {
                break@req
            }

            if (allRequiresMCRestart && !prop.requiresMcRestart()) {
                allRequiresMCRestart = false
            }

            if (allRequiresWorldRestart && !prop.requiresWorldRestart()) {
                allRequiresWorldRestart = false
            }
        }
    }

    return GuiConfig(
        parent,
        categories as List<IConfigElement>?,
        modId,
        allRequiresWorldRestart,
        allRequiresMCRestart,
        title
    )
}

/**
 * Localize a string.
 *
 * @since 1.1
 */
@Suppress("DEPRECATION")
fun String.localize(vararg args: Any): String =
    if (FMLCommonHandler.instance().side == Side.CLIENT) {
        net.minecraft.client.resources.I18n.format(this, args)
    } else {
        net.minecraft.util.text.translation.I18n.translateToLocalFormatted(this, args)
    }

/**
 * Run code on a specific [side].
 *
 * @since 1.3.1
 */
inline fun runSided(side: Side, block: () -> Unit) =
    if (side == FMLCommonHandler.instance().side) {
        block()
    } else {
        Unit
    }

/**
 * Run [client] when client side and [server] on server side.
 *
 * @since 1.3.1
 */
inline fun runSided(client: () -> Unit, server: () -> Unit) =
    if (FMLCommonHandler.instance().side.isClient) {
        client()
    } else {
        server()
    }

/**
 * Run code only on client side.
 *
 * @since 1.3.1
 */
inline fun runClientSide(block: () -> Unit) =
    if (FMLCommonHandler.instance().side.isClient) {
        block()
    } else {
        Unit
    }

/**
 * Run code only on server side.
 *
 * @since 1.3.1
 */
inline fun runServerSide(block: () -> Unit) =
    if (FMLCommonHandler.instance().side.isServer) {
        block()
    } else {
        Unit
    }

/**
 * Get the [capability] as a [Optional].
 * Check if it exists [Optional.isPresent].
 * Get the capability [Optional.get] and [Supplier.get]. The supplier is there since you could
 * compute the Capability on the spot and we wat to defer that until we actually want it.
 * The supplier will throw a [NullPointerException] if the [ICapabilityProvider.getCapability]
 * returns null. That should never happen unless it was changed prior go getting it.
 *
 * @example
 * ```kotlin
 * val stack: ItemStack
 * val optional = stack.capability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
 * if (optional.isPresent) {
 *   val itemHandler = optional.get().get()
 * }
 * ```
 *
 * @param capability The capability to get from the provider.
 * @param facing The side to check the capability on.
 * @param T The type of the capability.
 * @return The optional with or without the value.
 *
 * @since 1.3.1
 */
fun <T> ICapabilityProvider.capability(capability: Capability<T>, facing: EnumFacing? = null) =
    if (hasCapability(capability, facing)) {
        Optional.of(
            Supplier { getCapability(capability, facing) ?: throw NullPointerException() }
        )
    } else {
        Optional.empty()
    }
