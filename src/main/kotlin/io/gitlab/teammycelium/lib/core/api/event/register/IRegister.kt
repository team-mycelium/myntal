package io.gitlab.teammycelium.lib.core.api.event.register

interface IRegister : IRegisterBlocks, IRegisterItems, IRegisterRenders
