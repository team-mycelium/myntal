package io.gitlab.teammycelium.lib.core.common.items.tools

import io.gitlab.teammycelium.lib.core.common.localize
import net.minecraft.creativetab.CreativeTabs
import net.minecraft.item.ItemAxe
import net.minecraft.util.ResourceLocation
import net.minecraftforge.fml.common.Loader

class KAxe(material: ToolMaterial, damage: Float, speed: Float) : ItemAxe(material, damage, speed) {
    val name: String get() = "$unlocalizedName.name".localize()

    fun name(name: String, modId: String = Loader.instance().activeModContainer()?.modId ?: ""): KAxe {
        unlocalizedName = "$modId.$name"
        registryName = ResourceLocation(modId, name)

        return this
    }

    fun creativeTab(tab: CreativeTabs): KAxe {
        creativeTab = tab

        return this
    }
}