package io.gitlab.teammycelium.lib.core.api

import com.eclipsesource.json.JsonObject

interface IConfig {
    fun name(): String

    fun createConfig(): JsonObject
    fun setConfig(obj: JsonObject)
}