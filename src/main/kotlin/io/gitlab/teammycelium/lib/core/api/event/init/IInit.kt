package io.gitlab.teammycelium.lib.core.api.event.init

interface IInit: IInitCommon, IInitClient, IInitServer