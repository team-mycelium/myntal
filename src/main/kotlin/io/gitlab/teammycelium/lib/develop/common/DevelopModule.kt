package io.gitlab.teammycelium.lib.develop.common

import com.eclipsesource.json.JsonObject
import io.gitlab.teammycelium.lib.core.api.IModule
import io.gitlab.teammycelium.lib.core.common.Config
import io.gitlab.teammycelium.lib.core.common.Loader
import io.gitlab.teammycelium.lib.core.common.monads.Option
import io.gitlab.teammycelium.lib.develop.api.IFeature
import net.minecraft.block.Block
import net.minecraft.item.Item
import net.minecraftforge.client.event.ColorHandlerEvent
import net.minecraftforge.registries.IForgeRegistry
import org.apache.logging.log4j.LogManager

class DevelopModule : IModule, Loader<IFeature>("develop") {
    override val logger = LogManager.getLogger("MyceliumLib/Develop")!!

    private var config: Option<JsonObject> = Option.None()

    override var load: List<IFeature> = listOf()

    override fun name(): String {
        return "develop"
    }

    override fun createConfig(): JsonObject {
        logger.info("createConfig")

        val obj = JsonObject()

        load.forEach {
            val rarityObject = JsonObject()

            rarityObject.add(Config.Keys.ENABLED, true)

            rarityObject.add(Config.Keys.PARAMETERS, it.createConfig())

            obj.add(it.name(), rarityObject)
        }

        return obj
    }

    override fun setConfig(obj: JsonObject) {
        logger.info("setConfig")

        Config("mycelium-lib").config(load, obj)

        config = Option.Some(obj)
    }

    override fun registerBlocks(registry: IForgeRegistry<Block>) {
        logger.info("registerBlocks")
    }

    override fun registerBlockColors(event: ColorHandlerEvent.Block) {
        logger.info("registerBlockColors")
    }

    override fun registerItems(registry: IForgeRegistry<Item>) {
        logger.info("registerItems")
    }

    override fun registerItemColors(event: ColorHandlerEvent.Item) {
        logger.info("registerItemColors}")
    }

    override fun registerRenders() {
        logger.info("registerRenders")
    }
}