package io.gitlab.teammycelium.lib.develop.common.features

import com.eclipsesource.json.JsonObject
import io.gitlab.teammycelium.lib.develop.api.IFeature
import net.minecraftforge.common.MinecraftForge
import net.minecraftforge.fml.common.event.FMLInitializationEvent
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent
import net.minecraftforge.oredict.OreDictionary
import org.apache.logging.log4j.LogManager
import java.io.File
import java.util.EnumMap
import java.io.IOException
import java.io.FileWriter

class OreDictFeature : IFeature {
    private val logger = LogManager.getLogger("Myntal/Developer/OreDictionary")!!

    private val logDir = File("logs/mdpk-hlpr/")
    private val file = File(logDir, "ore-dict.md")
    private val oreDict = HashMap<String, EnumMap<OreDistCategory, MutableList<String>>>()

    override fun name(): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun createConfig(): JsonObject {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun setConfig(obj: JsonObject) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun preInit(event: FMLPreInitializationEvent) {
        if (!logDir.exists()) {
            logDir.mkdirs();
        }

        MinecraftForge.EVENT_BUS.register(this);
    }

    override fun init(event: FMLInitializationEvent) {
        // Not needed
    }

    override fun postInit(event: FMLPostInitializationEvent) {
        try {
            FileWriter(file).use { writer ->
                oreDict.forEach { (key, value) ->
                    writer.write("# $key")

                    writer.write(System.lineSeparator())
                    writer.write(System.lineSeparator())

                    value.forEach { (category, itemBlocks) ->
                        writer.write("## $category")

                        writer.write(System.lineSeparator())

                        writer.write("```")

                        writer.write(System.lineSeparator())

                        itemBlocks.forEach { itemBlock ->
                            writer.write(itemBlock)

                            writer.write(System.lineSeparator())
                        }

                        writer.write("```")

                        writer.write(System.lineSeparator())
                        writer.write(System.lineSeparator())
                    }
                }
            }
        } catch (ioe: IOException) {
            logger.error(ioe)
        }

    }

    override fun clientPreInit(event: FMLPreInitializationEvent) {
        // Not needed
    }

    override fun clientInit(event: FMLInitializationEvent) {
        // Not needed
    }

    override fun clientPostInit(event: FMLPostInitializationEvent) {
        // Not needed
    }

    override fun serverPreInit(event: FMLPreInitializationEvent) {
        // Not needed
    }

    override fun serverInit(event: FMLInitializationEvent) {
        // Not needed
    }

    override fun serverPostInit(event: FMLPostInitializationEvent) {
        // Not needed
    }

    @SubscribeEvent
    fun onOreRegister(event: OreDictionary.OreRegisterEvent) {
        val location = event.ore.item.registryName

        if (location != null) {
            val map = oreDict.getOrElse(location.resourceDomain) {
                EnumMap(OreDistCategory::class.java)
            }

            val name = event.name

            if (name.contains("Ore") || name.contains("ore")) {
                OreDistCategory.add(map, OreDistCategory.ORE, location.resourcePath)
            } else if (name.contains("Flower") || name.contains("flower")) {
                OreDistCategory.add(map, OreDistCategory.FLOWER, location.resourcePath)
            } else if (name.contains("Block") || name.contains("block")) {
                OreDistCategory.add(map, OreDistCategory.BLOCK, location.resourcePath)
            } else {
                OreDistCategory.add(map, OreDistCategory.OTHER, location.resourcePath)
            }
        }
    }

    private enum class OreDistCategory {
        BLOCK,
        FLOWER,
        ORE,
        OTHER;

        companion object {
            fun add(map: MutableMap<OreDistCategory, MutableList<String>>, category: OreDistCategory, path: String) {
                map.getOrElse(category) { mutableListOf() }.add(path)
            }
        }
    }
}
